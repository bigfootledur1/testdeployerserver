#!/bin/sh
dotnet publish -o tutoSite
zip tutoSite.zip tutoSite/*
zip dotnet-core-tutorial-version.zip tutoSite.zip aws-windows-deployment-manifest.json
